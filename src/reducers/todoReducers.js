const initialState = {
  todos: []

}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'LIST_TODO':
      return { ...state, todos: [...state.todos, payload] }
    default:
      return state
  }
}
