const initialState = {
  email: null,
  emailVerified: null,
  isConnected: false,
  photoURL: null,
  displayName: null,
  uid: null
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'IS_LOGIN':
      return { ...state, isConnected: state.isConnected = true, ...payload }
    case 'DISCONNECT':
      return { ...state, isConnected: state.isConnected = false }
    default:
      return state
  }
}
