import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Home from './Home'
import Signin from './auth/Signin'
import PrivateRoute from '../helpers/PrivateRoute'
import NotFound from './NotFound'
import Signup from './auth/Signup'
import ResetPassword from './auth/ResetPassword'

const App = () => {
  return (
    <Switch>
      <PrivateRoute exact path='/home' component={Home} />
      <Route exact path='/' component={Signin} />
      <Route exact path='/signup' component={Signup} />
      <Route exact path='/resetPassword' component={ResetPassword} />
      <Route path='*' component={NotFound} />
    </Switch>
  )
}

export default App
