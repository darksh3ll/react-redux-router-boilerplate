import React, { useState } from 'react'
import '../../styles/login.scss'
import { useDispatch } from 'react-redux'
import Firebase from '../../helpers/firebase'
import { Link, useHistory } from 'react-router-dom'
import Error from '../../components/Error'

const Signin = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const getSignup = (e) => {
    e.preventDefault()
    Firebase.auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        dispatch({ type: 'IS_LOGIN', payload: { ...res.user } })
        history.push('/home')
      }
      )
      .catch(error => setError(error.message))
  }

  return (
    <div className='container container-signin'>
      {error && <Error error={error} />}
      <form onSubmit={getSignup} className='container box-form-login  '>
        <h3>Login to your account</h3>
        <FormEmail email={email} setEmail={setEmail} />
        <FormPassword password={password} setPassword={setPassword} />
        <BtnLogin title='Login' />
        <Link to='/signup'>Don't have an account yet? Sign up</Link>
      </form>

    </div>
  )
}

const FormEmail = ({ email, setEmail }) => (
  <div className='form-group'>
    <label>Email address</label>
    <input
      autoFocus
      onChange={(e) => setEmail(e.target.value)}
      type='email'
      value={email}
      className='form-control'
      id='exampleInputEmail1'
      placeholder='Enter email'
    />
    <small
      id='emailHelp'
      className='form-text text-muted'
    >
            We'll never share your email with anyone else.
    </small>
  </div>
)

const FormPassword = ({ password, setPassword }) => (
  <div className='form-group'>
    <label>Password</label>
    <input
      onChange={(e) => setPassword(e.target.value)}
      value={password}
      type='password'
      className='form-control'
      id='exampleInputPassword1'
      placeholder='Password'
    />
    <Link to='/resetPassword'>Forgot your password?</Link>
  </div>
)

const BtnLogin = ({ title }) => (
  <button
    type='submit'
    className='btn btn-primary'
  >
    {title}
  </button>
)

export default Signin
