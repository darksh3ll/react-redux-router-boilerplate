import React, { useState } from 'react'
import Firebase from '../../helpers/firebase'
import '../../styles/resetPassword.scss'
import { Link } from 'react-router-dom'
import Error from '../../components/Error'
const ResetPassword = () => {
  const [email, setEmail] = useState('')
  const [error, setError] = useState('')

  const forgotPassword = () => {
    Firebase.auth().sendPasswordResetEmail(email)
      .then((res) => {
        console.log(res)
      }).catch((error) => {
        setError(error.message)
      })
  }

  return (
    <div className=' container container-resetPassword'>
      {error && <Error error={error} />}
      <h1>Forgotten password?</h1>
      <FormEmail email={email} setEmail={setEmail} />
      <button onClick={forgotPassword} className='btn btn-primary'>Submit</button>
      <Link to='/'>Cancel and return to home page</Link>
    </div>
  )
}

const FormEmail = ({ setEmail, email }) => (
  <div className='form-group'>
    <label>Email address</label>
    <input
      autoFocus
      onChange={(e) => setEmail(e.target.value)}
      type='email'
      value={email}
      className='form-control'
      id='exampleInputEmail1'
      placeholder='Enter email'
    />
    <small
      id='emailHelp'
      className='form-text text-muted'
    >
          Enter the email address with which you registered. We will send you an email with your username and a link to reset your password.
    </small>
  </div>
)

export default ResetPassword
