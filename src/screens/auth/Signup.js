import React, { useState } from 'react'
import '../../styles/login.scss'
import Firebase from '../../helpers/firebase'
import { Link } from 'react-router-dom'

const Signup = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const getSignup = (e) => {
    e.preventDefault()
    Firebase.auth()
      .createUserWithEmailAndPassword(email, password)
      .then((res) => {
        console.log(res)
      })
      .catch(error => console.log(error))
  }

  return (
    <div className='container-signin'>
      <form onSubmit={getSignup} className='container box-form-login'>
        <FormEmail email={email} setEmail={setEmail} />
        <FormPassword password={password} setPassword={setPassword} />
        <BtnLogin title='create an account' />
        <Link to='/'>Already have an account? Connect yourself</Link>
      </form>
    </div>

  )
}

const FormEmail = ({ email, setEmail }) => (
  <div className='form-group'>
    <h3>Create your account</h3>
    <label>Email address</label>
    <input
      autoFocus
      onChange={(e) => setEmail(e.target.value)}
      type='email'
      value={email}
      className='form-control'
      id='exampleInputEmail1'
      placeholder='Enter email'
    />
    <small
      id='emailHelp'
      className='form-text text-muted'
    >
            We'll never share your email with anyone else.
    </small>
  </div>
)

const FormPassword = ({ password, setPassword }) => (
  <div className='form-group'>
    <label>Password</label>
    <input
      onChange={(e) => setPassword(e.target.value)}
      value={password}
      type='password'
      className='form-control'
      id='exampleInputPassword1'
      placeholder='Password'
    />
  </div>
)

const BtnLogin = ({ title }) => (
  <button
    type='submit'
    className='btn btn-primary'
  >
    {title}
  </button>
)

export default Signup
