import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/notFound.scss'

const NotFound = () => {
  return (
    <div className='container_notFound'>
      <div>
        <i className='fas fa-exclamation-triangle' />
      </div>
      <h1>404</h1>
      <h1>Page not Found</h1>
      <Link to='/home'>Return to Home Page</Link>
    </div>
  )
}

export default NotFound
