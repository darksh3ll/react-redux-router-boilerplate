import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Firebase from '../helpers/firebase'

const Home = () => {
  const idUsers = useSelector(state => state.authReducers.uid)
  const userEmail = useSelector(state => state.authReducers.email)
  const db = Firebase.firestore().collection('todos')
  const dispatch = useDispatch()

  const [todo, setTodo] = useState('')

  useEffect(() => {
    return db
      .where('uid', '==', idUsers)
      .onSnapshot(querySnapshot => {
        querySnapshot.forEach(doc => {
          dispatch({ type: 'LIST_TODO', payload: { ...doc.data() } })
        })
      })
  }, [])

  const logout = () => {
    dispatch({ type: 'DISCONNECT' })
    window.location.href = '/'
  }

  async function addTodo () {
    await db.add({
      uid: idUsers,
      date: Date.now(),
      title: todo,
      complete: false
    })
  }

  return (
    <div>
      <button onClick={logout} className='btn btn-primary'>Deconnexion</button>
      <h1>Welcome:{userEmail}</h1>
      <div className='form-group'>
        <label>todos</label>
        <input
          onChange={(e) => setTodo(e.target.value)}
          value={todo}
          type='text'
          className='form-control'
          id='exampleInputPassword1'
          placeholder='todos'
        />
      </div>
      <button onClick={() => addTodo()}>Add</button>
    </div>

  )
}

export default Home
