import React from 'react'
import ReactDOM from 'react-dom'
import App from './screens/App'
import { BrowserRouter } from 'react-router-dom'
import * as serviceWorker from './serviceWorker'
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'

// import reducers
import authReducers from './reducers/authReducers'
import todoReducers from './reducers/todoReducers'
// ........
const store = createStore(
  combineReducers({
    authReducers,
    todoReducers

  }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
  , document.getElementById('root'))

serviceWorker.unregister()
