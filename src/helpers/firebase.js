import app from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyDmXhZCXjOgj587gVyP1mhw3r8UyrP5a68',
  authDomain: 'learnreactnative-28a17.firebaseapp.com',
  databaseURL: 'https://learnreactnative-28a17.firebaseio.com',
  projectId: 'learnreactnative-28a17',
  storageBucket: 'learnreactnative-28a17.appspot.com',
  messagingSenderId: '668277454797',
  appId: '1:668277454797:web:3425134378d59015197684',
  measurementId: 'G-7HT98G46B3'
}

const Firebase = app.initializeApp(config)
export default Firebase
